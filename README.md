cdi (change directory interactively)
====================================

Caveat: Code is a proper mess, but it works for me.


| Shortcut             | Description                                                            |
|----------------------|------------------------------------------------------------------------|
| `Ctrl-n`             |  Select next entry                                                     |
| `Ctrl-p`             |  Select previous entry                                                 |
| `Tab`                |  Toggle between bookmark view / directory view                         |
| `Esc` or `Ctrl-c`    |  Exit                                                                  |
|  `Return` or `Enter` |  Change directory to selected entry or first entry if none is selected |
| `Ctrl-f`             | Browse inside of selected folder if in directory view                  |
| `Ctrl-b`             | Browse inside of parent folder if in directory view                    |
| `Ctrl-r`             | Reload directory contents if in directory view                         |
| `Ctrl-w`             | Clear input                                                            |
