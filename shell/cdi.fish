#!/bin/fish
# https://gitlab.com/putputlaw/cdi

function cdi
    set dirfile (mktemp /tmp/cdi.$USER.XXXXXXXXXXXXX)
    cdi-select select --temp-file $dirfile $argv
    cd (cat $dirfile)
    rm $dirfile
end
