pub mod bm;
pub mod dir;

use std::path::PathBuf;

pub enum Selection {
    Here,
    Up,
    There(PathBuf),
    BestMatch,
    Index(usize),
}

pub trait DSet {
    type Item;

    fn peek(&self, direction: Selection) -> Option<Self::Item>;
    fn focus(&mut self, direction: Selection);
    fn list(&self) -> Vec<Self::Item>;
    fn set_pattern(&mut self, pattern: &str);
}
