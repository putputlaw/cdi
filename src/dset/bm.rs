use super::{DSet, Selection};
use simsearch::SimSearch;
use std::fs::{create_dir_all, read_to_string, write};
use std::path::PathBuf;

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, Eq)]
pub struct Bookmark {
    pub name: String,
    pub path: PathBuf,
}

pub struct Bookmarks {
    list: Vec<Bookmark>,
    focus: Option<Bookmark>,
    engine: SimSearch<Bookmark>,
    filtered_list: Vec<Bookmark>,
}

impl Bookmarks {
    pub fn new() -> Bookmarks {
        Bookmarks {
            list: vec![],
            focus: None,
            engine: SimSearch::new(),
            filtered_list: vec![],
        }
    }

    pub fn write(&self) {
        let config_file = initialise_bookmarks();
        write(
            config_file,
            serde_yaml::to_string(&self.list).expect("generating configuration file failed"),
        )
        .expect("unable to write config file. do you have the required permissions?")
    }
}

impl Default for Bookmarks {
    fn default() -> Bookmarks {
        let config_file = initialise_bookmarks();
        let contents = read_to_string(config_file).expect("could not read config file");
        let list = serde_yaml::from_str::<Vec<Bookmark>>(&contents)
            .expect("error while parsing config file");
        let mut engine = SimSearch::new();
        for bm in list.iter() {
            engine.insert(bm.clone(), &bm.name)
        }

        Bookmarks {
            list: list.clone(),
            focus: None,
            engine: engine,
            filtered_list: list,
        }
    }
}

impl DSet for Bookmarks {
    type Item = Bookmark;

    fn peek(&self, selection: Selection) -> Option<Self::Item> {
        match selection {
            Selection::Here => self.focus.clone(),
            Selection::Index(i) => self.filtered_list.get(i).cloned(),
            Selection::BestMatch => self.filtered_list.first().cloned(),
            _ => None,
        }
    }

    fn focus(&mut self, selection: Selection) {
        self.focus = self.peek(selection)
    }

    fn list(&self) -> Vec<Self::Item> {
        self.filtered_list.clone()
    }

    fn set_pattern(&mut self, pattern: &str) {
        self.filtered_list = self.engine.search(pattern);
    }
}

////////////////////////////////////////////////

fn initialise_bookmarks() -> PathBuf {
    let config_dir = dirs::config_dir().expect("no config dir").join("cdi");
    let config_file = config_dir.join("bookmarks.yaml");

    if !config_dir.exists() {
        create_dir_all(&config_dir)
            .expect("unable to create configuration directory. do you have sufficient rights?");
    }

    assert!(config_dir.is_dir());

    if !config_file.exists() {
        write(
            &config_file,
            serde_yaml::to_string(&Bookmarks::new().list)
                .expect("failed to serialize empty Bookmarks object"),
        )
        .expect("unable to write config file. do you have sufficient rights?");
    }

    config_file
}
