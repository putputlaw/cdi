use super::{DSet, Selection};
use simsearch::SimSearch;
use std::path::PathBuf;

pub struct Directory {
    /// location of currently focused directory
    focus: PathBuf,
    /// pattern for fuzzy search
    pattern: String,
    /// children of `focus` of type directory
    dirs: Vec<PathBuf>,
    /// children of `focus` not of type directory
    other: Vec<PathBuf>,
    /// filtered_dirs
    filtered_dirs: Vec<PathBuf>,
}

impl Directory {
    pub fn list_files(&self) -> &Vec<PathBuf> {
        &self.other
    }

    pub fn update(&mut self) {
        self.on_dirs_changed();
        self.on_pattern_changed();
    }

    fn on_dirs_changed(&mut self) {
        let mut all: Vec<PathBuf> = self
            .focus
            .read_dir()
            .expect("invalid directory")
            .filter_map(|e| e.ok())
            .map(|e| PathBuf::from(e.path().clone()))
            .collect();
        all.sort();
        self.dirs = all.iter().cloned().filter(|p| p.is_dir()).collect();
        self.other = all.iter().cloned().filter(|p| !p.is_dir()).collect();
        self.on_pattern_changed();
    }

    fn on_pattern_changed(&mut self) {
        if self.pattern.is_empty() {
            self.filtered_dirs = self.dirs.clone();
        } else {
            let mut engine: SimSearch<PathBuf> = SimSearch::new();
            for path in self.dirs.iter() {
                engine.insert(
                    path.clone(),
                    &path
                        .file_name()
                        .expect("no filename")
                        .to_string_lossy()
                        .into_owned(),
                )
            }
            self.filtered_dirs = engine.search(&self.pattern);
        }
    }
}

impl Default for Directory {
    fn default() -> Directory {
        let mut nav = Directory {
            focus: std::env::current_dir().expect("no current dir"),
            pattern: "".into(),
            dirs: vec![],
            other: vec![],
            filtered_dirs: vec![],
        };

        nav.on_dirs_changed();
        nav
    }
}

impl DSet for Directory {
    type Item = PathBuf;

    fn peek(&self, sel: Selection) -> Option<Self::Item> {
        Some(match sel {
            Selection::Here => self.focus.clone(),
            Selection::Up => match self.focus.parent() {
                Some(parent) => parent.into(),
                None => self.focus.clone(),
            },
            Selection::There(path) => {
                if path.is_dir() {
                    path.into()
                } else {
                    panic!("{:?} is not a directory", &path)
                }
            }
            Selection::BestMatch => {
                if !self.pattern.is_empty() && !self.filtered_dirs.is_empty() {
                    self.filtered_dirs[0].clone()
                } else {
                    self.focus.clone()
                }
            }
            Selection::Index(i) => {
                if self.filtered_dirs.len() > i {
                    self.filtered_dirs[i].clone()
                } else {
                    self.focus.clone()
                }
            }
        })
    }

    fn focus(&mut self, sel: Selection) {
        match sel {
            Selection::Here => {}
            _ => {
                self.focus = self.peek(sel).expect("<Directory as DSet>::peek() cannot return None").into();
                self.on_dirs_changed();
            }
        }
    }

    fn list(&self) -> Vec<Self::Item> {
        self.filtered_dirs.clone()
    }

    fn set_pattern(&mut self, pattern: &str) {
        self.pattern = pattern.into();
        self.on_pattern_changed();
    }
}
