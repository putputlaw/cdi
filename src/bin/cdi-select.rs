#[allow(dead_code)]
use std::io::{self, Write};
use std::path::PathBuf;

use structopt::StructOpt;
use termion::cursor::Goto;
use termion::event::Key;
use termion::input::MouseTerminal;
use termion::raw::IntoRawMode;
use termion::screen::AlternateScreen;
use tui::backend::TermionBackend;
use tui::layout::{Constraint, Direction, Layout};
use tui::style::{Color, Modifier, Style};
use tui::widgets::{Block, Borders, List, Paragraph, SelectableList, Text, Widget};
use tui::Terminal;
use unicode_width::UnicodeWidthStr;

use cdi::dset::bm::Bookmarks;
use cdi::dset::dir::Directory;
use cdi::dset::{DSet, Selection};
use cdi::util::event::{Config, Event, Events};

#[derive(PartialEq, Eq)]
enum View {
    Directory,
    Bookmarks,
}

/// App holds the state of the application
struct App {
    /// Current value of the input box
    input: String,
    view: View,
    nav: Directory,
    bms: Bookmarks,
    selected: Option<usize>,
    base_dir: PathBuf,
}

impl App {
    fn target(&self) -> PathBuf {
        match self.view {
            View::Bookmarks => self
                .selected
                .and_then(|i| self.bms.peek(Selection::Index(i)))
                .or(self.bms.peek(Selection::BestMatch))
                .map(|bm| bm.path)
                .unwrap_or(self.base_dir.clone()),
            View::Directory => self
                .selected
                .and_then(|i| self.nav.peek(Selection::Index(i)))
                .or(self.nav.peek(Selection::BestMatch))
                .expect("unexpected error"),
        }
    }

    fn update_selected(&mut self) {
        match self.view {
            View::Bookmarks => {
                if self.input.is_empty() || self.bms.list().is_empty() {
                    self.selected = None;
                } else {
                    self.selected = Some(0);
                }
            }
            View::Directory => {
                if self.input.is_empty() || self.nav.list().is_empty() {
                    self.selected = None;
                } else {
                    self.selected = Some(0);
                }
            }
        }
    }
}

impl Default for App {
    fn default() -> App {
        App {
            input: String::new(),
            view: View::Directory,
            nav: Directory::default(),
            bms: Bookmarks::default(),
            selected: None,
            base_dir: std::env::current_dir().expect("no current dir"),
        }
    }
}

#[derive(StructOpt)]
#[structopt(name = "cdi", about = "just a fancy cd")]
enum Options {
    List,
    Select {
        #[structopt(parse(from_os_str))]
        base_path: Option<PathBuf>,
        /// The selected path will be written into the specified temp file
        #[structopt(short = "t", long = "temp-file", parse(from_os_str))]
        temp_file: PathBuf,
    },
}

fn main() -> Result<(), failure::Error> {
    // Create default app state
    let mut app = App::default();

    let opt = Options::from_args();

    match opt {
        Options::List => {
            for bm in app.bms.list().iter() {
                println!("{:032}  {:?}", bm.name, bm.path);
            }
        }
        Options::Select {
            base_path,
            temp_file,
        } => {
            base_path.map(|p| app.nav.peek(Selection::There(p)));

            // drops terminal after this block
            {
                // Terminal initialization
                let stdout = io::stdout().into_raw_mode()?;
                let stdout = MouseTerminal::from(stdout);
                let stdout = AlternateScreen::from(stdout);
                let backend = TermionBackend::new(stdout);
                let mut terminal = Terminal::new(backend)?;

                // Setup event handlers
                let events = Events::with_config(Config {
                    tick_rate: std::time::Duration::from_millis(1000),
                    exit_key: Key::Ctrl('c'),
                });

                loop {
                    // Draw UI
                    terminal.draw(|mut f| {
                        // Layout
                        let _rects = Layout::default()
                            .direction(Direction::Vertical)
                            .margin(0)
                            .constraints([Constraint::Length(3), Constraint::Min(1)].as_ref())
                            .split(f.size());
                        let rect_input = _rects[0];
                        let _rects = Layout::default()
                            .direction(Direction::Vertical)
                            .margin(0)
                            .constraints([Constraint::Length(3), Constraint::Min(1)].as_ref())
                            .split(_rects[1]);
                        let rect_target = _rects[0];
                        let rect_output = _rects[1];
                        let _rects = Layout::default()
                            .direction(Direction::Horizontal)
                            .margin(0)
                            .constraints([Constraint::Percentage(50), Constraint::Min(1)].as_ref())
                            .split(_rects[1]);
                        let rect_left = _rects[0];
                        let rect_right = _rects[1];

                        // Widgets
                        Paragraph::new([Text::raw(&app.input)].iter())
                            .style(Style::default().fg(Color::Yellow))
                            .block(Block::default().borders(Borders::ALL).title("Search"))
                            .render(&mut f, rect_input);

                        Paragraph::new(
                            [Text::raw(app.target().to_string_lossy().into_owned())].iter(),
                        )
                        .style(Style::default().fg(Color::Yellow))
                        .block(Block::default().borders(Borders::ALL).title("Target"))
                        .render(&mut f, rect_target);

                        let style = Style::default().fg(Color::White).bg(Color::Black);
                        match app.view {
                            View::Directory => {
                                SelectableList::default()
                                    .block(
                                        Block::default().borders(Borders::ALL).title("Directories"),
                                    )
                                    .items(
                                        &app.nav
                                            .list()
                                            .iter()
                                            .map(|p| {
                                                p.file_name()
                                                    .expect("no filename")
                                                    .to_string_lossy()
                                                    .into_owned()
                                            })
                                            .collect::<Vec<String>>(),
                                    )
                                    .select(app.selected)
                                    .style(style)
                                    .highlight_style(
                                        style.fg(Color::LightGreen).modifier(Modifier::BOLD),
                                    )
                                    .highlight_symbol("▶")
                                    .render(&mut f, rect_left);

                                List::new(app.nav.list_files().iter().map(|p| {
                                    Text::raw(p.file_name().expect("no filename").to_string_lossy())
                                }))
                                .block(Block::default().borders(Borders::ALL).title("Other"))
                                .render(&mut f, rect_right);
                            }
                            View::Bookmarks => {
                                SelectableList::default()
                                    .block(
                                        Block::default().borders(Borders::ALL).title("Directories"),
                                    )
                                    .items(
                                        &app.bms
                                            .list()
                                            .iter()
                                            .map(|p| p.name.clone())
                                            .collect::<Vec<String>>(),
                                    )
                                    .select(app.selected)
                                    .style(style)
                                    .highlight_style(
                                        style.fg(Color::LightGreen).modifier(Modifier::BOLD),
                                    )
                                    .highlight_symbol("▶")
                                    .render(&mut f, rect_output);
                            }
                        }
                    })?;

                    // Put the cursor back inside the input box
                    write!(
                        terminal.backend_mut(),
                        "{}",
                        Goto(2 + app.input.width() as u16, 2)
                    )?;
                    // stdout is buffered, flush it to see the effect immediately when hitting backspace
                    io::stdout().flush().ok();

                    let num_candidates = match app.view {
                        View::Bookmarks => app.bms.list().len(),
                        View::Directory => app.nav.list().len(),
                    };

                    // Handle input
                    match events.next()? {
                        Event::Input(input) => match input {
                            Key::Ctrl('c') | Key::Esc => {
                                std::fs::write(
                                    temp_file,
                                    &std::env::current_dir()
                                        .expect("no current dir")
                                        .to_string_lossy()
                                        .into_owned(),
                                )?;
                                return Ok(());
                            }
                            Key::Char('\n') => {
                                break;
                            }
                            Key::Ctrl('n') => {
                                app.selected = match app.selected {
                                    Some(i) => Some((i + 1) % num_candidates),
                                    None if num_candidates > 0 => Some(0),
                                    _ => None,
                                }
                            }
                            Key::Ctrl('p') => {
                                app.selected = match app.selected {
                                    Some(i) if i == 0 => Some(num_candidates - 1),
                                    None if num_candidates > 0 => Some(num_candidates - 1),
                                    Some(i) => Some(i - 1),
                                    _ => None,
                                }
                            }
                            Key::Ctrl('r') => {
                                if app.view == View::Directory {
                                    app.nav.update();
                                    app.update_selected();
                                }
                            }
                            Key::Char('\t') => match app.view {
                                View::Bookmarks => app.view = View::Directory,
                                View::Directory => app.view = View::Bookmarks,
                            },
                            Key::Char(c) => {
                                app.input.push(c);
                                app.nav.set_pattern(&app.input);
                                app.bms.set_pattern(&app.input);
                                app.update_selected();
                            }
                            Key::Backspace => {
                                app.input.pop();
                                app.nav.set_pattern(&app.input);
                                app.bms.set_pattern(&app.input);
                                app.update_selected();
                            }
                            Key::Ctrl('w') => {
                                app.input = "".into();
                                app.nav.set_pattern(&app.input);
                                app.bms.set_pattern(&app.input);
                                app.update_selected();
                            }
                            Key::Ctrl('f') => {
                                if app.view == View::Directory {
                                    app.nav.focus(Selection::There(app.target()));
                                    app.input = "".into();
                                    app.nav.set_pattern(&app.input);
                                    app.update_selected();
                                }
                            }
                            Key::Ctrl('b') => {
                                if app.view == View::Directory {
                                    app.nav.focus(Selection::Up);
                                    app.input = "".into();
                                    app.nav.set_pattern(&app.input);
                                    app.update_selected();
                                }
                            }
                            _ => {}
                        },
                        Event::Tick => {}
                    }
                }
            }

            match app.view {
                View::Directory => {
                    std::fs::write(temp_file, &app.target().to_string_lossy().into_owned())?
                }
                View::Bookmarks => {
                    let candidates = app.bms.list();
                    if let Some(bm) = app.selected.map(|i| &candidates[i]).or(candidates.first()) {
                        std::fs::write(temp_file, &bm.path.to_string_lossy().into_owned())?
                    }
                }
            }
            // io::stdout().flush().ok();
        }
    }
    Ok(())
}
