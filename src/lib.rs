#[macro_use]
extern crate serde_derive;

pub mod util;

/// searcheable set of directories
pub mod dset;
